package com.example.appfriday8.models

data class AppModels(
    val courses: List<Course>,
    val topic: List<Topic>
)