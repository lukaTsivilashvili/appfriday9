package com.example.appfriday8.models

data class Topic(
    val color: String,
    val duration: Int,
    val title: String,
    val type: String
)