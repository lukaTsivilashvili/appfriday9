package com.example.appfriday8.network

import com.example.appfriday8.models.AppModels
import retrofit2.Response
import retrofit2.http.GET

interface ApiInterface {
    @GET("v3/29db8caa-95cb-44be-aa3c-eee0aa406870")
    suspend fun getCountry(): Response<AppModels>
}