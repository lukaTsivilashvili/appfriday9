package com.example.appfriday8.adapters


import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.appfriday8.databinding.ItemTopBinding
import com.example.appfriday8.models.Topic

class RecyclerTopAdapter(private val context:Context) :
    RecyclerView.Adapter<RecyclerTopAdapter.ItemViewHolder>() {
    val topic = mutableListOf<Topic>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val itemView =
            ItemTopBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        val holder = ItemViewHolder(itemView)
        return holder
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = topic.size

    inner class ItemViewHolder(private val binding: ItemTopBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private lateinit var model: Topic

        fun bind() {
            model = topic[adapterPosition]

            binding.introduceTv.text = model.title
            binding.minsTv.text = model.duration.toString() + "Mins"
            binding.root.setBackgroundColor(ContextCompat.getColor(context, ))

        }
    }

    fun setData(topic: MutableList<Topic>) {
        this.topic.addAll(topic)
        notifyDataSetChanged()
    }

    fun clearData() {
        this.topic.clear()
        notifyDataSetChanged()
    }

}
