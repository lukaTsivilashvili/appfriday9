package com.example.appfriday8.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.appfriday8.models.AppModels
import com.example.appfriday8.network.RetrofitInstance
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class HomeViewModel : ViewModel() {

    private val courseLiveData = MutableLiveData<AppModels>().apply {
        mutableListOf<AppModels>()
    }

    val _courseLiveData: LiveData<AppModels> = courseLiveData


    fun init() {
        CoroutineScope(Dispatchers.IO).launch {
            getPhotos()
        }
    }


    private suspend fun getPhotos() {
        val result = RetrofitInstance.service.getCountry()

        if (result.isSuccessful) {
            val items = result.body()
            courseLiveData.postValue(items)
        }
    }

}