package com.example.appfriday8.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.appfriday8.adapters.RecyclerTopAdapter
import com.example.appfriday8.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {

    private lateinit var binding: FragmentHomeBinding
    private lateinit var myAdapter: RecyclerTopAdapter
    private val viewModel: HomeViewModel by viewModels()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentHomeBinding.inflate(inflater, container, false)
        init()
        return binding.root

    }

    private fun init() {
        viewModel.init()
        initRecycler()
        observe()

    }


    private fun initRecycler() {
        myAdapter = RecyclerTopAdapter()
        binding.recyclerTop.layoutManager = GridLayoutManager(requireActivity(), 2)
        binding.recyclerTop.adapter = myAdapter
    }

    private fun observe() {

        viewModel._courseLiveData.observe(viewLifecycleOwner, {
            myAdapter.setData(it.topic.toMutableList())
        })
    }

}